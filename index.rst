:auto-console: true
:title: How not to be a game designer
:data-transition-duration: 0
:skip-help: true
:css: hovercraft.css

.. title: How Not to Be a Game Designer

----

How Not to Be a Game Designer
=============================

by: Craig Maloney

Presented at Penguicon 2021

``http://decafbad.net``

----

Introduction
============

"Hi, I'm Craig Maloney"

----

I love games
============

----

I've wanted to be a game designer since I learned about computer games
======================================================================

----

.. image:: static/popular_computing_chris_crawford.jpg

----

So, if this is something that I've wanted a long time then I HAVE to have a portfolio of games, right?
======================================================================================================

----

Well...
=======

-----

.. image:: static/Charlie_and_Sid_-_A_Dog\'s_Life_1918_-_PDVD_039.jpeg

----

What we'll cover
================

Advice and feelings that tripped me up

How to focus on the important pieces

Recommendations

----

What we'll ignore
=================

Marketing / Kickstarter

(I have no experience here)

----

What we'll ignore
=================

Publishing and finding a publisher 

(I also have no experience here)

----

What we'll ignore
=================

Getting a job as a game designer

(seeing a pattern here?)

----

The Structure
=============

The Fallacy

The Truth

The Remedy

Extra Credit

----

Failing at Game Design
=======================

(a crash course that took me 20+ years to unlearn)

----

The Fallacy
===========

Wait until you're a good enough designer to design good games

----

.. image:: static/941px-The_Navigator_-_A_History_of_the_Movies.jpg

----

The Truth
=========

Nobody has ever started off designing good games

Nobody

----

The Remedy
==========

You get better at game design by designing games

----

Extra Credit
============

Design 15 games and compare your first game to your 15th game.

Then keep going.

----

The Fallacy
===========

You can learn all there is to know about good game design from books, videos, courses, and so on.

----

.. image:: static/Sherlock-Jr.jpg

----

The Truth
=========

No book can teach you everything about game design

-----

An Aside
========

Which is better for learning how to play a game: reading the manual or playing the game?

----

The Remedy
==========

Pick one book, video, or course.

Stick with it for at least a month.

If the material isn't not working out for you, pick another.

(There is no repeat)

----

The Remedy
==========

Resist the urge to shop around and collect 'em all

(Unless you're interested in becoming a game design collector)

----

(Note: Becoming a game design collector is not as cool as it sounds)

----

Extra Credit
============

Note: I'll have a link to a short list of books I recommend at the end of the presentation.

----

The Fallacy
===========

Your ideas aren't any good

----

.. image:: static/Charlie-Chaplin-City-Lights-1931-02.jpg

----

The Truth
=========

Your ideas are your own.

Some will be crap, some will be good, and many will be unworkable in their current state, but they're all your ideas

----

Aside
=====

The difference between an idea and a great game is the execution of the idea

----

Another Aside
=============

Sometimes an idea can be the springboard for another idea

----

The Remedy
==========

Write down your ideas, and review them from time to time. 

Take the ones that still resonate with you and try to make them real

----

Extra Credit
============

Write down five ideas each day for a week and publish them

You're not lacking ideas

----

The Fallacy
===========

If it's difficult to do then you shouldn't be doing it

----

.. image:: static/Modern_Zamanlar_Filmi.jpg

----

The Truth
=========

Everything is difficult when you start out

----

The Aside
=========

Fear, Uncertainty and Doubt

The designer's closest companions

----

The Remedy
==========

Get comfortable with uncertainty

----

The Remedy
==========

Difficulty means you're pushing your limits

----

Extra Credit
============

Be gentle with yourself, but press on

----

The Fallacy
===========

Comparing ourselves to others

----

.. image:: static/Chaplin_Charlie_His_New_Job_01.jpg

----

The Truth
=========

We're all unique. Comparing ourselves to others is foolish

----

The Remedy
==========

Recognize your unique voice and gifts

Recognize that everyone has a different path, and yours is unique to you

----

Extra Credit
============

Be courageous and awesome

(Yes, I'm still working on this one)

----

The Advice
==========

Play lots of games, and play lots of different games

----

What I heard
============

Buy all the games

----

.. image:: static/17429463471_7352989f40_c.jpg

https://www.flickr.com/photos/iqremix/17429463471/

----

The Admonition
==============

Games you don't play are expensive doorstops.

(Trust me on this one)

----

The Remedy
==========

Find games to play with others 

Be OK with letting go of games that you're not going to play or no longer interest you

----

Corollary
=========

Sometimes it takes a while to realize you don't have any interest in a particular game field

It's OK to not like every single game genre out there

----

Extra Credit
============

Keep your gaming radar open

Keep your shelves even more open

----

The Advice
==========

Iterate, Iterate, Iterate

----

The Truth
=========

(Actually this is good advice.) 

----

The Extra Credit
================

Game design is about making creative guesses, testing those guesses, and making adjustments.

Keep iterating.

----

The Fallacy
===========

You must create everything yourself or it isn't game design

----

The Truth
=========

Anything where you are creating the game you want to exist in the world is game design.

----

Advice
======

Use the tools, engines, and rules that help you make your games. 

----

Extra Credit
============

Build a game using an existing, openly-licensed engine, and release it.

----

Extra Extra Credit
==================

Build a level or a bunch of levels for an existing game


----

The Fallacy
===========

The market should decide what games you'll make

----

.. image:: static/Foire_au_Jambon,_Paris_1914_(1).jpg

----

The Truth
=========

It depends...

----

The Truth
=========

The marketplace is always changing and unpredictable

----

The Truth
=========

Advice that worked in 1990 isn't true for 2020

(unless it is)

----

The Truth
=========

Most classic games defined their genre and their market, not the other way around.

----

The Remedy
==========

Make the games you want to make and let the market worry about itself.

----

Reminder
========

(I haven't sold a game.)

----

The Fallacy
===========

You can finish that game in 6 months

----

The Truth
=========

...

----

The Advice
==========

Some designs take longer than others.

Understand that it's a process.

----

Extra Credit
============

Sign up for a "game jam" so you have a due date.

----

Extra Extra Credit
==================

Notice your feelings during a game jam or contest and notice your uncertainty around it.

----

The Fallacy
===========

Playtest the early stages of your designs with your friends and family

----

.. image:: static/Roscoe_Arbuckle,_Sybil_Seely_&_Buster_Keaton_-_Oct_1920_EH.jpg

----

The Truth
=========

There's a reason why JoDee calls some of my games "Cones of Dunshire".

----

The Advice
==========

Play your designs by yourself to iterate over them.

Write up the rules.

Iterate until they're solid.

*Then* playtest them.

----

Extra Credit
============

Post your designs up in game design forums to get some additional playtesting.

Head to Metatopia to have game designers and players playtest your designs.

----

The Fallacy
===========

If it's not *x* then it's not worth making

----

The Truth
=========

Whatever you want to make is worth making

----

The Remedy
==========

It's a relief that *x* exists because you don't have to support it

----

The Remedy
==========

It's also great that you have *x* as a model for what you might do differently

----

Extra Credit
============

What would you make if *x* didn't exist?

----

Extra Extra Credit
==================

What would you make that would be the inverse of *x*?

----

The Fallacy
===========

Games need a lot of polish

----

.. image:: static/Buster_Keaton_as_a_bellboy_in_the_March_18_1918_movie_The_Bell_Boy.gif

----

The Truth
=========

Games do need polish, but they need to be a game first

----

The Remedy
==========

Start with the game first, then polish

----

The Fallacy
===========

Games are a multi-billion dollar business

You will make lots of money.

----

.. image:: static/charlie_chaplin_pockets.jpg

----

The Truth
=========

Not all games make money.

----

The Remedy
==========

Drop the expectation of making large sums of money on your games

----

The Admonition
==============

If it makes money then great

If it doesn't make money that doesn't mean it's not good

----

Extra Credit
============

Play to find out what happens

----

The Fallacy
===========

I'm not a game designer until *x* occurs

----

The Truth
=========

You *are* a game designer

----

Extra Credit
============

Go out and make some games

----

Extra Extra Credit
==================

Send your game to me

I can't guarantee I'll play it, but I'll definitely read it

----

Thank you
=========

----

The book list
=============

``http://decafbad.net/games/game-design-books``
